import numpy as np
import sklearn
import matplotlib.pyplot as plt

# Read data
fData = open("brain_and_body_weight.txt", "r")
line = fData.readline()

# Parse data
data = []
while(len(line)) :
    list = line.split()
    data.append([float(list[1]), float(list[2])])
    line = fData.readline()
fData.close()

# Remove unusual datasets
data = [[a,b] for a,b in data if a < 1000 and b < 1000]
X = np.array([[a for a,b in data]]).T
y = np.array([[b for a,b in data]]).T

# Visualize data
plt.plot(X, y, 'ro')
plt.xlabel('Brain (kg)')
plt.ylabel('Weight (kg)')
plt.show()

# Building Xbar
one = np.ones((X.shape[0], 1))
Xbar = np.concatenate((one, X), axis = 1)

# Calculating weights of the fitting line
A = np.dot(Xbar.T, Xbar)
b = np.dot(Xbar.T, y)
w = np.dot(np.linalg.pinv(A), b)
print('w = ', w)
# Preparing the fitting line
w_0 = w[0][0]
w_1 = w[1][0]
x0 = np.linspace(0, 550, 2)
y0 = w_0 + w_1*x0

# Drawing the fitting line
plt.plot(X.T, y.T, 'ro')     # data
plt.plot(x0, y0)               # the fitting line
plt.xlabel('Brain (kg)')
plt.ylabel('Weight (kg)')
plt.show()

y1 = w_1*3.5 + w_0

print( u'Predict weight of body with 3.5 kg weight of brain %.2f'  %(y1) )
print( u'Actual weight of body with 3.5 kg weight of brain %.2f'  %([b for a,b in data if a == 3.5][0]) )

from sklearn import datasets, linear_model

# fit the model by Linear Regression
regr = linear_model.LinearRegression(fit_intercept=False) # fit_intercept = False for calculating the bias
regr.fit(Xbar, y)

# Compare two results
print( 'Predict weight of body with 3.5 kg weight of brain using scikit-learn: ', regr.predict(np.array([[1, 3.5]])))
print( 'Predict weight of body with 3.5 kg weight of brain using w matrix: ', y1)


