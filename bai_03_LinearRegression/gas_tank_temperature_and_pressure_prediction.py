import pandas as pd
import matplotlib.pyplot as plt
import sklearn

# Read data
data = pd.read_csv("gas_tank_temperature_and_pressure.csv")
print("Columns: ")
print(data.columns)

# Rename columns
data = data.drop("Index", axis = 1)
cols = "Tank_temp Petrol_temp Initial_tank_pressure Petrol_pressure Hydrocarbons".split()
data.rename(columns = dict(zip(data.columns, cols)), inplace = True)

# Generate training set
train = data.sample(frac = 0.8, random_state = 1)

# Select anything not in the training set and put it in test
test = data.loc[~data.index.isin(train.index)]

target = "Hydrocarbons"
cols.remove(target)

from sklearn.linear_model import LinearRegression

LR = LinearRegression()
LR.fit(data[cols], data[target])

predictions = LR.predict(test[cols])
print("Predicted value: \n", predictions)
print("Actual value: \n", list(test[target]))
