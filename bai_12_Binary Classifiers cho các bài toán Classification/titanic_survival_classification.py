import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.linear_model import Perceptron
from sklearn.preprocessing import LabelEncoder

train = pd.read_csv("survival_train.csv")
print("======= train data =======")
print(train)
print("\n")

print("======= columns =======")
print(train.columns)
print("\n")

print("======= info =======")
print(train.info())
print("\n")

print("====== Remove non-numeric data that's not related ======")
rm_cols = "PassengerId Name Ticket Cabin Embarked".split()
train.drop(columns = rm_cols, inplace = True)
print(train.columns)
print("\n")

print("We need to keep Sex feature in this case because it might effect the output")
print("So we gonna label Sex feature since it is non-numeric value")
labelEncoder = LabelEncoder()
labelEncoder.fit(train['Sex'])
train['Sex'] = labelEncoder.transform(train['Sex'])
print("\n")

print("======= info again =======")
print(train.info())
print("======= Good =======\n")


print("====== Lets see if the training data has any empty values =====")
print(train.isna().sum())
print("\n")

print("====== Filling empty data with column mean value ======")
train.fillna(train.mean(), inplace=True)
print("\n")

print("====== After filling empty data =====")
print(train.isna().sum())

print("\nNice!!!!! Lets start the training step")
print("We're gonna use PLA to try to classify the train dataset into 2 classes (binary classification)")
print("After that with the passenger info in test dataset we will try to predict whether the passenger survived or not")

pla = Perceptron(tol=1e-3, random_state=0)
X = train.drop(columns = "Survived")
y = train.drop(columns = [c for c in train.columns if c not in "Survived"])
pla.fit(X, y.values.ravel())
print(pla.score(X,y))

del train

print("\nDid you see that? Yeah, we did it. Though its not a good result")
print("The number tells us about the mean accuracy on the test data and labels")
print("So lets try to become a God and judge our passengers. RIP titanic")

print("=" * 100)

test = pd.read_csv("survival_test.csv")
print("Well do the something as same as the previous train data")

print("======= test data =======")
print(test)
print("\n")

print("======= columns =======")
print(test.columns)
print("\n")

print("======= info =======")
print(test.info())
print("\n")

print("====== Remove non-numeric data that's not related ======")
test.drop(columns = rm_cols, inplace = True)
print(test.columns)
print("\n")

print("We need to keep Sex feature in this case because it might effect the output")
print("So we gonna label Sex feature since it is non-numeric value")
labelEncoder.fit(test['Sex'])
test['Sex'] = labelEncoder.transform(test['Sex'])
print("\n")

print("======= info again =======")
print(test.info())
print("======= Good =======\n")


print("====== Lets see if the training data has any empty values =====")
print(test.isna().sum())
print("\n")

print("====== Filling empty data with column mean value ======")
test.fillna(test.mean(), inplace=True)
print("\n")

print("====== After filling empty data =====")
print(test.isna().sum())

print("\n")
print("Predict time!!!")
print(pla.predict(test))

print("Congratulation! You have achived a bunch of stupid binaries")

