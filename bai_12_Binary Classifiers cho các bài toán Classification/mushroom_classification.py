import numpy as np
import pandas as pd
from sklearn.linear_model import Perceptron
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt

data = pd.read_csv("mushrooms.csv")
print("====== head ======")
print(data.head())
print("\n")

print("====== info ======")
print(data.info())
print("\n")

print("====== isna sum ======")
print(data.isna().sum())
print("\n")

for cols in data.columns.values:
    labelEncode = LabelEncoder()
    labelEncode.fit(data[cols])
    data[cols] = labelEncode.transform(data[cols])

train = data.sample(frac = 0.8, random_state=1)
test = data.loc[~data.index.isin(train.index)]

pla = Perceptron(tol=1e3, random_state=0)
X = train[[c for c in data.columns.values if c not in "class"]]
y = train["class"]
pla.fit(X, y)
print(pla.score(X, y))

y_predicted = pla.predict(test[[c for c in data.columns.values if c not in "class"]])
print(accuracy_score(test["class"], y_predicted.tolist()))


