import sys
import nltk
import sklearn
import pandas as pd
import numpy as np

## 1.Load the Dataset

# Load the dataset of sms messages
df = pd.read_table("SMSSpamCollection", header=None, encoding='utf-8')

# print useful information about the dataset

print(df.info())
print(df.head())

# check class distribution

classes = df[0]
print(classes.value_counts())

## 2. Preprocess the Data
# convert class labels to binary values, 0 = ham, 1 = spam

from sklearn.preprocessing import LabelEncoder

encoder = LabelEncoder()
Y = encoder.fit_transform(classes)

print(classes[:10])
print(Y[:10])

# store the SMS message data
text_messages = df[1]
print(text_messages[:10])

# use regex to replace email addr, urls, phone numbers, other numbers, symbols

# replace mail addr with 'emailaddr'
processed = text_messages.str.replace(r'^.@[^\.].*\.[a-z]{2,}$', 'emailaddr')

# replace urls with 'webaddress'
processed = processed.str.replace(r'^http[\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$', 'webaddress')

# replace money sympbols with 'moneysymb'
processed = processed.str.replace(r'£|\$', 'moneysymb')

# replace 10 digit phone numbers with ' phonenumber'
processed = processed.str.replace(r'^\(?[\d]{3}\)?[\s-]?[\d]{3}[\s-]?[\d]{4}$', 'phonenumbr')

# replace normal numbers with 'numbr'
processed = processed.str.replace(r'\d+(\.\d+)?', 'numbr')

# remove punctuation
processed = processed.str.replace(r'[^\w\d\s]', ' ')

# replace whitespace between terms with a single space
processed = processed.str.replace(r'\s+', ' ')

# remove leading and trailing whitespace
processed = processed.str.replace(r'^\s+|\s+?$', '')

# change word to lower case - Hello, HELLO, hello are all the same word!
processed = processed.str.lower()
print(processed)

# remove stop words from text messages

from nltk.corpus import stopwords

stop_words = set(stopwords.words('english'))

processed = processed.apply(lambda x: ' '.join(term for term in x.split() if term not in stop_words))

# remove word stems using a Porter stemmer
ps = nltk.PorterStemmer()

processed = processed.apply(lambda x: ' '.join(ps.stem(term) for term in x.split()))

print(processed)

from nltk.tokenize import word_tokenize

# creating a bag-of-words
all_words = []

for message in processed:
    words = word_tokenize(message)
    for w in words:
        all_words.append(w)

all_words = nltk.FreqDist(all_words)

# print the total number words and the 15 most common words

print('Number of words: {}'.format(len(all_words)))
print('Most common words: {}'.format(all_words.most_common(15)))

# use the 1500 most common words as features
word_features = list(all_words.keys())[:1500]


# define a find_features function
def find_features(message):
    words = word_tokenize(message)
    features = {}
    for word in word_features:
        features[word] = (word in words)
    return features


# Lets see an example
features = find_features(processed[0])
for key, value in features.items():
    if value == True:
        print(key)

# find features for all messages
messages = list(zip(processed, Y))

# define a seed for reproducibility
seed = 1
np.random.seed = seed
np.random.shuffle(messages)

# call find features function for each SMS messages
featuresets = [(find_features(text), label) for (text, label) in messages]

# split training and testing data sets using sklearn
from sklearn import model_selection

training, testing = model_selection.train_test_split(featuresets, test_size=0.25, random_state=seed)

print('Training: {}'.format(len(training)))
print('Testing: {}'.format(len(testing)))

## 4. Scikit-Learn Classifiers with NLTK
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.metrics import classification_report, accuracy_score, confusion_matrix

# Define models to train
names = ['K Nearest Neighbors', 'Decision Tree', 'Random Fores', 'LogisticRegression', 'SGD Classifier', 'Naive Bayes',
         'SVM Linear']
classifiers = [
    KNeighborsClassifier(),
    DecisionTreeClassifier(),
    RandomForestClassifier(),
    LogisticRegression(),
    SGDClassifier(max_iter=100),
    MultinomialNB(),
    SVC(kernel='linear')
]

models = []
for i in range(len(names)):
    models.append((names[i], classifiers[i]))

# wrap models in NLTK
from nltk.classify.scikitlearn import SklearnClassifier

# ensemble method - Voting classifier
from sklearn.ensemble import VotingClassifier

nltk_ensemble = SklearnClassifier(VotingClassifier(estimators = models, voting='hard', n_jobs=-1))
nltk_ensemble.train(training)
accuracy = nltk.classify.accuracy(nltk_ensemble, testing) * 100
print('Voting Classifier: Accuracy: {}'.format(accuracy))

# make class label prediction for testing set
txt_features, labels = zip(*testing)

prediction = nltk_ensemble.classify_many(txt_features)

# print a  confusion matrix and a classification report
print(classification_report(labels, prediction))

print(pd.DataFrame(
    confusion_matrix(labels, prediction),
    index=[['actual', 'actual'], ['ham', 'spam']],
    columns=[['predicted', 'predicted'], ['ham', 'spam']]))
