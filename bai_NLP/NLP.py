import nltk
import sys
import sklearn
from nltk.tokenize import sent_tokenize, word_tokenize

text = 'Hello students, how are you doing today? The olympics are inspiring, and Python is awesome. You look great today.'
print(sent_tokenize(text))
print(word_tokenize(text))

# removing stop words - useless data
from nltk.corpus import stopwords

print(set(stopwords.words('english')))

example = 'This is some sample text, showing off the stop words filtration.'

stop_words = set(stopwords.words('english'))

word_tokens = word_tokenize(example)

sentence = [w for w in word_tokens if w not in stop_words]

filtered_sentence = []

for w in word_tokens:
    if w not in stop_words:
        filtered_sentence.append(w)

print(word_tokens)
print(filtered_sentence)
print(sentence)

# Stemming words with NLTK
from nltk.stem import PorterStemmer

ps = PorterStemmer()

example_words = ['ride', 'riding', 'rider', 'ride']

for w in example_words:
    print(ps.stem(w))

# Stemming an entire sentence
new_text = 'When riders are riding their horses, they often think of how cowboys rode horses.'

words = word_tokenize(new_text)

for w in words:
    print(ps.stem(w))

from nltk.corpus import udhr

print(udhr.raw('English-Latin1'))

from nltk.corpus import state_union
from nltk.tokenize import PunktSentenceTokenizer

train_text = state_union.raw('2005-GWBush.txt')
sample_text = state_union.raw('2006-GWBush.txt')

# Now that we have some test, we can train the PunktSentenceTokenizer

custom_sent_tokenizer = PunktSentenceTokenizer(train_text)

# Now lets tokenize the sample test
tokenized = custom_sent_tokenizer.tokenize(sample_text)


# Define a function that will tag each tokenized word with a part of speech
def process_content():
    try:
        for i in tokenized[:2]:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)

        # combine the part of speech tag with a regular expression
        chunkGram = r"""Chunk : {<RB.?>*<NNP>+<NN>?}"""
        chunkParser = nltk.RegexpParser(chunkGram)
        chunked = chunkParser.parse(tagged)

        # print the nltk tree
        for subtree in chunked.subtrees(filter=lambda t: t.label() == 'Chunk'):
            print(subtree)
        # draw the chunk with nltk
        # chunked.draw()

    except Exception as e:
        print(str(e))


process_content()


# Chinking with NLTK
def process_content():
    try:
        for i in tokenized[0]:
            words = nltk.word_tokenize(i)
            tagged = nltk.pos_tag(words)
            namedEnt = nltk.ne_chunk(tagged, binary = False)
        
    except Exception as e:
        print(str(e))


process_content()
