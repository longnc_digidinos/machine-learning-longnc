# Read more at https://www.datacamp.com/community/tutorials/k-means-clustering-python

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler
import seaborn as sns

train_path = "survival_train.csv"
train = pd.read_csv(train_path)

print("***** Train_Set *****")
print(train.head())
print("\n")

print("***** Column name *****")
print(train.columns.values)

print()
print("*****the train set after filling empty data*****")
print(train.isna().sum())
print("\n")
# Fill missing values with mean column values in the train set
train.fillna(train.mean(), inplace=True)

# Dataset after filling data
print()
print("*****In the train set*****")
print(train.isna().sum())
print("\n")

# remove non-numeric columns
train = train.drop(['Name','Ticket', 'Cabin','Embarked'], axis=1)

# labeling Sex feature since it is non-numeric value
labelEncoder = LabelEncoder()
labelEncoder.fit(train['Sex'])
train['Sex'] = labelEncoder.transform(train['Sex'])

# training data
X = np.array(train.drop(['Survived'], 1).astype(float))
y = np.array(train['Survived'])
kmeans = KMeans(n_clusters=2)
kmeans.fit(X)

correct = 0
for i in range(len(X)):
    predict_me = np.array(X[i].astype(float))
    predict_me = predict_me.reshape(-1, len(predict_me))
    prediction = kmeans.predict(predict_me)
    if prediction[0] == y[i]:
        correct += 1

print("\nCorrect rate of clustering train data: ", correct/len(X))
